const productList = [
	{
		id: "wdc001",
		image: "",
		productName: "Acoustic Guitar",
		description: "item description here",
		price: 6000,
		onOffer: true
	},
	{
		id: "wdc002",
		image: "",
		productName: "Electronic keyboard",
		description: "item description here",
		price: 15000,
		onOffer: true
	},
	{
		id: "wdc003",
		image: "",
		productName: "Electric Bass",
		description: "item description here",
		price: 9999,
		onOffer: true
	},
	{
		id: "wdc004",
		image: "",
		productName: "Electric Guitar (Gibson)",
		description: "item description here",
		price: 18000,
		onOffer: true
	},
	{
		id: "wdc005",
		image: "",
		productName: "Electric Guitar (Fender)",
		description: "item description here",
		price: 15500,
		onOffer: true
	},
	{
		id: "wdc006",
		image: "",
		productName: "Drum kit w/ Double Pedals",
		description: "item description here",
		price: 7800,
		onOffer: true
	},
	{
		id: "wdc007",
		image: "",
		productName: "Drum kit (snare, cymbal, bass)",
		description: "item description here",
		price: 4699,
		onOffer: true
	}
]

export default productList;
