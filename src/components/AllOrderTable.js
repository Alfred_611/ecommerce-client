import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { useState, useEffect, Fragment } from 'react';
import React from 'react';
import Swal from 'sweetalert2';


export default function AllOrderTable({orderProp}) {
	const {_id, purchasedOn, productName, totalAmount} = orderProp;
	
		return (
			<div>
					<Table striped bordered hover className="mt-2">
					  
					  <tbody>
					    <tr>
					      <td>{_id}</td>
					      <td>{purchasedOn}</td>
					      <td>{productName}</td>
					      <td>{totalAmount}</td>
					    </tr>
					  </tbody>
					</Table>
			</div>
			)
};