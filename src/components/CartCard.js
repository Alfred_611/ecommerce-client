import React from 'react';
import { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card, ButtonGroup, Button, ButtonToolbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductDetails from '../pages/ProductDetails';



export default function CartCard({cartProp}) {
	console.log({cartProp});
	const userId = localStorage.getItem('id');
	const {productId, quantity, price, subtotal, name, description, image, total} = cartProp;


	return (
		<Card className="m-3 bg-light">
		  <Card.Body>
		  	
		    <Card.Title className="mb-0">Product ID:</Card.Title>
		    <Card.Text>{productId}</Card.Text>
		    <Card.Title className="mb-0">Product Name:</Card.Title>
		    <Card.Text>{name}</Card.Text>
		    <Card.Img style={{width: "8rem"}} className="imageProduct mb-3" src={image} fluid/>
		    <Card.Title className="mb-0">Description:</Card.Title>
		    <Card.Text>{description}</Card.Text>
		    <Card.Title className="mb-0">Price:</Card.Title>
		    <Card.Text>PHP {price}</Card.Text>
		    <Card.Title className="mb-2">Quantity:</Card.Title>
		    <ButtonToolbar className=" thquantity buttongroup mr-2 mb-2" aria-label="Toolbar with button groups">
				    <ButtonGroup className=" mr-3" aria-label="Second group">
				       	    <Button variant="info"> - </Button> 
				    </ButtonGroup>
				       		  	{quantity}
				    <ButtonGroup className=" ml-3" aria-label="Third group">
				       	    <Button variant="info"> + </Button>
				    </ButtonGroup>
			</ButtonToolbar>
		    <Card.Title className="mb-0">Subtotal:</Card.Title>
		    <Card.Text>{subtotal}</Card.Text>
		    <Link className="btn btn-success" to={`/products`}>Checkout</Link>

		  </Card.Body>
		</Card>

	)
}


CartCard.propTypes = {
	cart: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
	}) 
}
