import PropTypes from 'prop-types';
import { Row, Col, Card, Button, CardGroup, Container, ButtonGroup, ButtonToolbar  } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductCard({productProp}) {
	const {_id, name, image, description, price} = productProp;
	const [ product, setProduct ] = useState([]);

	const addToYourCart = () => {
		let id =this._id;
		let name = this.name;
		let image = this.image;
		let description = this.description;
		let price = this.price;
		setProduct (id, name, image, description, price);
	}

		return (
		
			<Col xs={12} md={6} lg={3}  className="mt-3">
						<Card className="p-2" style={{width: "14rem"}}>
							<Card.Img  className="imageProduct" variant="top" src={image} />   
							<Card.Body>

									<Card.Title>{name}</Card.Title>
									<Card.Text>Price: PHP {price}</Card.Text>
									<Card.Text>{description}</Card.Text>
								<div className="d-flex justify-content-center">
								<ButtonToolbar className=" thquantity buttongroup mr-2 mb-2" aria-label="Toolbar with button groups">
										<ButtonGroup className="mr-1" aria-label="Second group">
											<Button variant="success"> Edit </Button> 
										</ButtonGroup>
										<ButtonGroup className="ml-1" aria-label="Third group">
											<Button variant="danger"> Delete </Button>
										</ButtonGroup>
								</ButtonToolbar>
								</div>
							</Card.Body>
						</Card>
			</Col>
			)
}

