import { Row, Col, Card, Image } from 'react-bootstrap';
export default function Highlights(){
	return (




		<Row className="mt-2 mb-3">
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1550590774-adc439e48ac6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid /> 
				<Card className="cardHighlight p-2 bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Guitars</h4>
				    </Card.Title>
				    <Card.Text>
				      Our collection of guitars span a wide range of types from acoustic to electric, classical to resonators, differing body shapes and sizes to perfectly suit your genre and playing style.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1608663003920-757dd225d6c5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid />
				<Card className="cardHighlight p-2 bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Percussion</h4>
				    </Card.Title>
				    <Card.Text>
				      We offer drum kit packages that differ in components: may it be a simple snare-bass-cymbals setup or kits with multiple toms and double pedals, we make sure that you find the one that suits you best.
				    </Card.Text>
				    	
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1599494009395-5b43c783a2d5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1587&q=80" fluid />
				<Card className="cardHighlight p-2 bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Keyboards</h4>
				    </Card.Title>
				    <Card.Text>
				      Our collection of high-quality electronic keyboards and synthesizers will surely inspire the artist in you. Check our catalogue to match your preferred size, color, setup and functionality.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
			<Image src="https://images.unsplash.com/photo-1590571054052-eec7ecae1f05?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80" fluid />
				<Card className="cardHighlight p-2 bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Others</h4>
				    </Card.Title>
				    <Card.Text>
				      Amplifiers and soundboards will complement your playing style on-the-go or in a studio. Find additional equipment and peripherals to resonate that perfect tone.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}