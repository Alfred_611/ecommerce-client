import React from 'react';
import { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import OrderHistory from '../pages/OrderHistory';



export default function OrderTable({orderProp}) {
	console.log({orderProp});
	const userId = localStorage.getItem('id');
	const {_id, purchasedOn, productName, totalAmount} = orderProp;
	

	return (
			<div>
					<h2 className="mt-4">My Order History</h2>
					<Table striped bordered hover className="mt-2">
					  <thead>
					    <tr>
					      <th><div className="d-flex justify-content-center">Order ID</div></th>
					      <th><div className="d-flex justify-content-center">Purchase Date</div></th>
					      <th><div className="d-flex justify-content-center">Item/s</div></th>
					      <th><div className="d-flex justify-content-center">Total</div></th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>{_id}</td>
					      <td>{purchasedOn}</td>
					      <td>{productName}</td>
					      <td>{totalAmount}</td>
					    </tr>
					  </tbody>
					</Table>
			</div>
			)
};