import React, { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardGroup } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar';
import CartCard from '../components/CartCard';


export default function CartView() {
	
	const [cart, setCart] = useState([]);
	const [cartItems, setCartItems] = useState([]);
	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`https://hidden-harbor-98173.herokuapp.com/carts/${userId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);			
			
			setCartItems(data[0].products.map(cartItem => {
				
				return(
					<CartCard key={cartItem._id} cartProp={cartItem} />
					)
				}
			))
			console.log(cartItems);
			
		})
	}, [])

	const checkOut = async () => {

	}

	return(
	<Fragment>
		<Row className="spacer"></Row>

		<Card>{cartItems}</Card>
	</Fragment>
	)
}
